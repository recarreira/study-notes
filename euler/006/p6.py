# -*- coding: utf-8 -*-

'''
The sum of the squares of the first ten natural numbers is,

12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten
natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one
hundred natural numbers and the square of the sum.
'''

sum_of_squares = 0
sum_of_numbers = 0


for number in xrange(1, 101):
    sum_of_numbers += number
    sum_of_squares += number * number

square_of_sum_of_numbers = sum_of_numbers * sum_of_numbers

print "sum of numbers: {0}".format(sum_of_numbers)
print "sum of squares: {0}".format(sum_of_squares)
print "square of sum of numbers: {0}".format(square_of_sum_of_numbers)
print "Difference: {0}".format(square_of_sum_of_numbers - sum_of_squares)
