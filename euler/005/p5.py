# -*- coding: utf-8 -*-

'''
2520 is the smallest number that can be divided by each of the numbers
from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all
of the numbers from 1 to 20?
'''
NUMBERS = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2]


def main():
    number = 20
    while is_divisible_by_all(number) is False:
        number += 20
    print number


def is_divisible_by_all(number):
    for divisor in NUMBERS:
        if not number % divisor == 0:
            return False
    return True


if __name__ == '__main__':
    main()
