# -*- coding: utf-8 -*-

'''
A palindromic number reads the same both ways. The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
'''

import re

regex = r"^(?P<first>\d)(?P<second>\d)(?P<third>\d)(?P=third)(?P=second)(?P=first)$"
regex_palindrome = re.compile(regex)
LOWEST_NUMBER_TO_MULTIPLY = 100


def main():
    print find_biggest_palindrome()


def find_biggest_palindrome():
    first_number = 999
    second_number = 999
    palindromic_numbers = []

    while first_number > LOWEST_NUMBER_TO_MULTIPLY:
        number = first_number * second_number
        if regex_palindrome.match(str(number)):
            palindromic_numbers.append(number)
        second_number -= 1
        if second_number == LOWEST_NUMBER_TO_MULTIPLY:
            first_number -= 1
            second_number = 999

    palindromic_numbers.sort()
    return palindromic_numbers[-1]

if __name__ == '__main__':
    main()
