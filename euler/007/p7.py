'''
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13
we can see that the 6th prime is 13.

What is the 10 001st prime number?
'''
prime_numbers = []


def main():
    print "Result: {0}".format(get_the_last_of_x_prime_numbers(10001))


def get_the_last_of_x_prime_numbers(position):
    candidate = 1
    count_of_prime_numbers = 0

    while count_of_prime_numbers < position:
        candidate += 1
        if is_prime(candidate):
            count_of_prime_numbers += 1
            prime_numbers.append(candidate)
    return candidate


def is_prime(number):
    for number_candidate in range(2, number):
        if number % number_candidate == 0:
            return False
    return True


if __name__ == '__main__':
    main()
