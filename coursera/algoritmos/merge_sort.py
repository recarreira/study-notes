import sys

def merge_sort(list):
    if len(list) == 1:
        return list
    else:
        l1 = list[:len(list)/2]
        l2 = list[len(list)/2:]
        a = merge_sort(l1)
        b = merge_sort(l2)
        return (merge(a,b))

def merge(a,b):
    c = []
    i = 0
    j = 0
    k = 0
    n = len(a) + len(b)
    for k in range(n):
        if i < len(a) and j < len(b):
            if a[i] < b[j]:
                c.append(a[i])
                i += 1
            else:
                c.append(b[j])
                j +=1
        elif i < len(a) and j >= len(b):
            c.append(a[i])
            i += 1
        elif i >= len(a) and j < len(b):
            c.append(b[j])
            j += 1
    return c

def main(argv):
    array = argv
    print "array: {0}".format(array)
    ordered = merge_sort(array)
    print "ordered: {0}".format(ordered)

if __name__ == "__main__":
   main(sys.argv[1:])