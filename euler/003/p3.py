'''
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

'''

prime_factors = []


def main():
    print "Result: {0}".format(get_bigger_prime_factor(600851475143))
    print "Prime factors: {0}".format(prime_factors)


def get_bigger_prime_factor(big_number):
    candidate = 2

    while big_number > 1:
        if not big_number % candidate == 0:
            candidate += 1
        elif is_prime(candidate):
            big_number /= candidate
            prime_factors.append(candidate)
    return candidate


def is_prime(number):
    for number_candidate in range(2, number):
        if number % number_candidate == 0:
            return False
    return True


if __name__ == '__main__':
    main()
