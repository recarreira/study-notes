'''
By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
'''

first = 1
second = 2
sum = 2
number = 0

while number < 4000000:
    number = first + second
    if number % 2 == 0:
        sum += number
    first, second = second, number

print "Result:"
print sum
